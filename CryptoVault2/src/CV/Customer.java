package CV;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;

public class Customer{
    String name = "";
    String phone = "";
    double balance = 0;
    boolean check = false;
    public void infocustomer(String ph) 
    {
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/acp?characterEncoding=latin1&useConfigs=maxPerformance","root",""); 
            Statement stmt=con.createStatement();
            String sql = String.format("select name, phone, balance from account where phone = '%s'", ph);  
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                name = rs.getString("name");
                phone = rs.getString("phone");
                balance = rs.getDouble("balance");
            }
            
        }
        catch(Exception e)
        {System.out.println(e);}
        
    }
    public void deleteacc(String ph)
    {
        PreparedStatement pst = null;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/acp?characterEncoding=latin1&useConfigs=maxPerformance","root",""); 
            Statement stmt=con.createStatement();
            String sql = String.format("select phone from account where phone = '%s'", ph);  
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                phone = rs.getString("phone");
            }
            
            int result = JOptionPane.showConfirmDialog(null, "Delete account?", "Confirm", JOptionPane.YES_NO_OPTION, ERROR_MESSAGE);
       
            if (result == 0)
            {
                String sql2 = String.format("delete from account where phone = '%s'", ph);  
                pst=con.prepareStatement(sql2);
                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Account deleted!", "Success", INFORMATION_MESSAGE);
                check = true;
            }
            
        }
        catch(Exception e)
        {System.out.println(e);}
        
        
        
    }
    public String getlname()
    {
        return name;
    }
    public String getlphone()
    {
        return phone;
    }      
    public double getlbalance()
    {
        return balance;
    }
    public boolean getcheck()
    {
        return check;
    }
}
