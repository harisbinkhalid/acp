package CV;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;
import static javax.swing.JOptionPane.WARNING_MESSAGE;

public class Deposit {
    String name = "";
    String Sphone = "";
    double Sbalance = 0;
    double amount = 0;
    Email se = new Email();
    public void infodeposit(String ph) 
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/acp?characterEncoding=latin1&useConfigs=maxPerformance","root","");
            Statement stmt = con.createStatement();
            String sql = String.format("select name, phone, balance from account where phone = '%s'", ph);  
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                name = rs.getString("name");
                Sphone = rs.getString("phone");
                Sbalance = rs.getDouble("balance");
            }
            
        }
        catch(Exception e)
        {System.out.println(e);}
        
    }
    public void depositamount(String recphone)
    {
        String recname = "";
        double recbalance = 0;
        String recemail = "";
        int rectype = 0;
        String checkphone = "";
        String password = "";
        PreparedStatement pst = null;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/acp?characterEncoding=latin1&useConfigs=maxPerformance","root","");
            Statement stmt = con.createStatement();
            String sql = String.format("select name, phone, password, email, type, balance from account where phone = '%s'", recphone);  
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                recname = rs.getString("name");
                rectype = rs.getInt("type");
                recbalance = rs.getDouble("balance");
                checkphone = rs.getString("phone");
                password = rs.getString("password");
                recemail = rs.getString("email");
            }
           
            
            if (recphone.equals(""))
            {
                JOptionPane.showMessageDialog(null, "Enter number!", "Warning", WARNING_MESSAGE);
            }
            else if (recphone.length() != 11)
            {
                JOptionPane.showMessageDialog(null, "Wrong number!", "Error", ERROR_MESSAGE);
            }
            else if (recphone.equals(Sphone))
            {
                JOptionPane.showMessageDialog(null, "Cannot transfer to yourself!", "Error", ERROR_MESSAGE);
            }
            else if (recphone.equals(checkphone))
            {
                if (amount == 0)
                {
                    JOptionPane.showMessageDialog(null, "Enter amount!", "Warning", WARNING_MESSAGE);
                }
                else if (amount < 0)
                {
                    JOptionPane.showMessageDialog(null, "Cannot send negative!", "Error", ERROR_MESSAGE);
                }
                else if ( amount > Sbalance)
                {
                    JOptionPane.showMessageDialog(null, "Not enough balance!", "Error", ERROR_MESSAGE);
                }
                else
                {   
                    int ran = (int)Math.floor(Math.random()*(9999-1000+1)+1000);
                    Sms sm = new Sms();
                    sm.sendsms(checkphone, ran);
                    
                    String checkpassword = "";
                    checkpassword =JOptionPane.showInputDialog(null, "Enter Password", "Confirm user", PLAIN_MESSAGE);
                    
                    
                    String ranpass = String.valueOf(ran);
                    
                    if (ranpass.equals(checkpassword))
                    {
                        ran = 0;
                        ranpass = "";
                        int result = JOptionPane.showConfirmDialog(null, "Send " + amount + " to " + recname, "Confirm", JOptionPane.YES_NO_OPTION, QUESTION_MESSAGE);
                        if (result == 0)
                        {
                            try
                            {
                                double samount = Sbalance;
                                samount -= amount;
                                String sql1 = String.format("update account set balance = '"+samount+"' where phone = '"+Sphone+"'");  
                                pst=con.prepareStatement(sql1);
                                pst.executeUpdate();
                                double ramount = recbalance;
                                ramount += amount;
                                String sql2 = String.format("update account set balance = '"+ramount+"' where phone = '"+recphone+"'");  
                                pst=con.prepareStatement(sql2);
                                pst.executeUpdate();
                                
                                String tx =  name + " deposit " + amount + " to your account: " + recname;
                                se.mail(recemail, "password", tx);
                                
                                JOptionPane.showMessageDialog(null, "Transfer complete!", "Success", INFORMATION_MESSAGE);
                            }
                            catch(Exception e)
                            {System.out.println(e);}
                        }
                    }
                    else if (checkpassword.isEmpty())
                    {
                        
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Password wrong!", "Error", ERROR_MESSAGE);
                    }
                }             
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Account not found!", "Error", ERROR_MESSAGE);
            }
        }
        catch(Exception e)
        {System.out.println(e);}
    }
    public String getlname()
    {
        return name;
    }
    public String getlphone()
    {
        return Sphone;
    }      
    public double getlbalance()
    {
        return Sbalance;
    }
    public void setamount(double a)
    {
       amount = a;
    }
}
